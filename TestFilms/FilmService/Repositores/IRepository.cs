﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;

namespace FilmService.Repositores
{
    public interface IRepository<T>
        where T : class
    {
        IEnumerable<T> All();//получение всех элементов
        T Get(int key);// получение одного элемента по id
        void Insert(T entity);//создание нового элемента
        void Update(T entity);// обновление элемента
        void SaveChanges();//сохранение изменений
    }
}
