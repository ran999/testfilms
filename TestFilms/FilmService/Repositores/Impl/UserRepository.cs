﻿using FilmService.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FilmService.Repositores.Impl
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(FilmContext context) : base(context)
        {
        }
        public IEnumerable<User> GetAll()
        {
            return All();
        }
        public User GetUser(int id)
        {
            return Get(id);
        }
        public void AddUser (User user)
        {
            Insert(user);
        }
        public User GetUser(string login)
        {
            return Queryable().FirstOrDefault(x => x.Login == login);
        }
        public User GetByEmailAndPassword(string Login, string Password)
        {
            var result = Queryable().FirstOrDefault(x => x.Login == Login && x.Password == Password);
            return result;
        }
        public void Save()
        {
            SaveChanges();
        }
    }
}
