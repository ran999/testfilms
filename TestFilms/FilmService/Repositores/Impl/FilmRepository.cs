﻿using FilmService.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace FilmService.Repositores.Impl
{
    public class FilmRepository : Repository<Film>, IFilmRepository
    {
        public FilmRepository(FilmContext context) : base(context)
        {
        }

        public IEnumerable<Film> GetAllFilms()
        {
            return All();
        }
        public void InsertFilm(Film film)
        {
            Insert(film);
        }
        public void UpdateFilm(Film film)
        {
            Update(film);
        }
        public Film GetFilm(int id)
        {
            return Queryable().AsNoTracking().FirstOrDefault(x => x.Id == id);
        }
        public void Save()
        {
            SaveChanges();
        }
    }
}
