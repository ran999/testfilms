﻿using FilmService.DB;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace FilmService.Repositores.Impl
{
    public class Repository<T> : IRepository<T>
        where T : class
    {
        FilmContext database;
        public Repository(FilmContext context)
        {
            database = context;
        }

        protected IQueryable<T> Queryable()
        {
            return database.Set<T>();
        }

        public virtual IEnumerable<T> All()
        {
            return Queryable().ToList();
        }

        public virtual T Get(int key)
        {
            return database.Set<T>().Find(key);
        }


        public virtual void Insert(T entity)
        {
            database.Set<T>().Add(entity);
        }

        public virtual void Update(T entity)
        {
            database.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public void SaveChanges()
        {
            database.SaveChanges();
        }
    }
}
