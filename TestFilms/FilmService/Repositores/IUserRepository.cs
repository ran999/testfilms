﻿using FilmService.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Repositores
{
    public interface IUserRepository : IRepository<User>
    {
        IEnumerable<User> GetAll();
        User GetUser(int id);
        void AddUser(User user);
        User GetUser(string login);
        User GetByEmailAndPassword(string Login, string Password);
        void Save();
    }
}
