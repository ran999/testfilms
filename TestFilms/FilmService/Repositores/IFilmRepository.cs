﻿using FilmService.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Repositores
{
    public interface IFilmRepository : IRepository<Film>
    {
        IEnumerable<Film> GetAllFilms();
        void InsertFilm(Film film);
        void UpdateFilm(Film film);
        Film GetFilm(int id);
        void Save();
    }
}
