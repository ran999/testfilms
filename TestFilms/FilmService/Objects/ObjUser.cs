﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Objects
{
    public class ObjUser
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
