﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;

namespace FilmService.DB
{
    public class FilmContext:DbContext
    {
        DbSet<Film> Films { get; set; }
        DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<FilmContext>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
