﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.DB
{
    public class Film
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public string Producer { get; set; }
        public string User { get; set; }
        public string ImgPath { get; set; }
    }
}
