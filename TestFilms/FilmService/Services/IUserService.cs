﻿using FilmService.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Services
{
    public interface IUserService
    {
        void AddUser(ObjUser user);
        ObjUser GetUser(int id);
        ObjUser GetUser(string login);
        ObjUser Login(string login, string password);
    }
}
