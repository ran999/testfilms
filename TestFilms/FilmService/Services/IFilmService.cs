﻿using FilmService.Objects;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Services
{
    public interface IFilmService
    {
        void CreateFilm(ObjFilm obj);
        void UpdateFilm(ObjFilm obj);
        ObjFilm GetFilm(int id);
        List<ObjFilm> GetAllFilms();
    }
}
