﻿using FilmService.DB;
using FilmService.Objects;
using FilmService.Repositores;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Services.Impl
{
    public class FilmService : IFilmService
    {
        private IFilmRepository _filmRepository { get; set; }

        public FilmService(IFilmRepository filmRepository)
        {
            _filmRepository = filmRepository;
        }
        public void CreateFilm(ObjFilm film)
        {
            var model = Map(film);
            _filmRepository.InsertFilm(model);
            _filmRepository.Save();
        }
        public void UpdateFilm(ObjFilm film)
        {
            var model = Map(film);
            _filmRepository.UpdateFilm(model);
            _filmRepository.Save();
        }
        public ObjFilm GetFilm(int id)
        {
            Film film = _filmRepository.GetFilm(id);
            return Map(film);
        }
        public List<ObjFilm> GetAllFilms()
        {
            var FilmList = _filmRepository.GetAllFilms();
            var ObjFilmList = new List<ObjFilm>();
            foreach (var film in FilmList)
            {
                ObjFilmList.Add(Map(film));
            }
            return ObjFilmList;
        }
        #region Map
        public ObjFilm Map(Film film)
        {
            if (film == null)
                return null;

            ObjFilm objFilm = new ObjFilm
            {
                Id = film.Id,
                Title = film.Title,
                Description = film.Description,
                Year = film.Year,
                Producer = film.Producer,
                User = film.User,
                ImgPath = film.ImgPath
            };
            return objFilm;
        }
        private Film Map(ObjFilm objFilm)
        {
            Film film = new Film
            {
                Id = objFilm.Id,
                Title = objFilm.Title,
                Description = objFilm.Description,
                Year = objFilm.Year,
                Producer = objFilm.Producer,
                User = objFilm.User,
                ImgPath = objFilm.ImgPath
            };
            return film;
        }
        #endregion
    }
}
