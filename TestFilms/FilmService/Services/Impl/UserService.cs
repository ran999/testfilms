﻿using FilmService.DB;
using FilmService.Objects;
using FilmService.Repositores;
using System;
using System.Collections.Generic;
using System.Text;

namespace FilmService.Services.Impl
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository { get; set; }

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public void AddUser(ObjUser user)
        {
            var model = Map(user);
            _userRepository.AddUser(model);
            _userRepository.Save();
        }
        public ObjUser GetUser(int id)
        {
            User user = _userRepository.GetUser(id);
            return Map(user);
        }
        public ObjUser GetUser(string login)
        {
            User user = _userRepository.GetUser(login);
            return Map(user);
        }
        public ObjUser Login(string login, string password)
        {
            ObjUser model = null;
            var User = _userRepository.GetByEmailAndPassword(login, password);
            model = Map(User);
            return model;
        }
        #region Map
        public ObjUser Map(User user)
        {
            if (user == null)
                return null;

            ObjUser objUser = new ObjUser
            {
                Id = user.Id,
                Login = user.Login,
                Password = user.Password
            };
            return objUser;
        }
        public User Map(ObjUser user)
        {
            if (user == null)
                return null;

            User objUser = new User
            {
                Id = user.Id,
                Login = user.Login,
                Password = user.Password
            };
            return objUser;
        }
        #endregion
    }
}
