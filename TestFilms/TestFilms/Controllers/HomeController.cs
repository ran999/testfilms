﻿using FilmService.Objects;
using FilmService.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace TestFilms.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        IFilmService _filmService { get; set; }

        public HomeController(IFilmService filmService)
        {
            _filmService = filmService;
        }
        public ActionResult Index(int? page)
        {
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            List<ObjFilm> films = null;
            films = _filmService.GetAllFilms();
            
            return View(films.ToPagedList(pageNumber, pageSize));
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(ObjFilm objFilm, HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                string[] type = System.IO.Path.GetFileName(upload.FileName).Split('.');
                string fileName = String.Format(@"{0}.{1}", System.Guid.NewGuid(),type[type.Length-1]);
                upload.SaveAs(Server.MapPath("~/Images/" + fileName));
                objFilm.ImgPath = fileName;
            }
            objFilm.User = HttpContext.User.Identity.Name;
            _filmService.CreateFilm(objFilm);
            return RedirectToAction("/Index");
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (_filmService.GetFilm(id).User== HttpContext.User.Identity.Name)
                return View(_filmService.GetFilm(id));
            else
            {
                TempData["Error"] = "Вы не можете отредактировать данную запись.";
                return RedirectToAction("/Details/" + id);
            }
        }
        [HttpPost]
        public ActionResult Edit(ObjFilm objFilm, HttpPostedFileBase upload)
        {
            if (_filmService.GetFilm(objFilm.Id).User == HttpContext.User.Identity.Name)
            {
                if (upload != null)
                {
                    string fileName = objFilm.ImgPath;
                    System.IO.File.Delete(Server.MapPath("~/Images/" + fileName));
                    string[] type = System.IO.Path.GetFileName(upload.FileName).Split('.');
                    fileName = String.Format(@"{0}.{1}", System.Guid.NewGuid(), type[type.Length - 1]);
                    upload.SaveAs(Server.MapPath("~/Images/" + fileName));
                    objFilm.ImgPath = fileName;
                }
                _filmService.UpdateFilm(objFilm);
                return RedirectToAction("/Details/"+objFilm.Id);
            }
            else
            {
                TempData["Error"] = "Вы не можете отредактировать данную запись.";
                return RedirectToAction("/Details/" + objFilm.Id);
            }
        }
        public ActionResult Details(int id)
        {
            ObjFilm film = _filmService.GetFilm(id);
            return View(film);
        }
    }
}