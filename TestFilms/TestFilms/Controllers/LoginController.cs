﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FilmService.Objects;
using FilmService.Services;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Security;
using System.Net;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security;

namespace TestFilms.Controllers
{
    public class LoginController : Controller
    {
        private IUserService _userService { get; set; }
        public LoginController(IUserService userService)
        {
            _userService = userService;
        }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(ObjUser user)
        {

            if (ModelState.IsValid)
            {
                var User = _userService.Login(user.Login, user.Password);

                if (User != null)
                {
                    ClaimsIdentity claim = new ClaimsIdentity("ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                    claim.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(), ClaimValueTypes.String));
                    claim.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login, ClaimValueTypes.String));
                    claim.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                        "OWIN Provider", ClaimValueTypes.String));
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    

                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Некоректные логин и (или) пароль");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Reg()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Reg(ObjUser user,string checkpass)
        {
            if (user.Password!=checkpass)
            {
                TempData["Error"] = "Пароли не совпадают.";
                return RedirectToAction("Reg", "Login");
            }
            if (ModelState.IsValid)
            {
                var User = _userService.GetUser(user.Id);

                if (User == null)
                {
                    _userService.AddUser(user);
                    return RedirectToAction("Index", "Home");
                }
                else
                ModelState.AddModelError("", "Пользователь с таким логином существует");
            }
            return View();
        }
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}